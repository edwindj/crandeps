# R package dependency

`dependencies.R` reads the CRAN info and creates a csv file with dependency counts for all CRAN packages. 

The resulting stats are used to generate badges:

e.g. 

- `https://tinyverse.netlify.com/badge/Rcpp`: !["Rcpp dependencies"](https://tinyverse.netlify.com/badge/Rcpp)


seealso: https://tinyverse.netlify.com